var syncVersion = 1;

function checkErrorResponse(jqXHR, status, errorThrown) {
  try {
    var data = JSON.parse(jqXHR.responseText);
  } catch (ex) {
    console.error(jqXHR, status, errorThrown);
    alert('Error while making request. See JavaScript console for more details.');
    return;
  }
  if (data.error) {
    if (jqXHR.statusCode == 404 && data.error == 'no_data') {
      $('#textarea').val('');
      console.warn('Server response:', data);
      alert('Warning: no data is stored on the server.');
    } else if (jqXHR.status == 403 && data.error == 'invalid_token') {
      console.error('Server response:', data);
      alert('Error: invalid token.');
    } else if (jqXHR.status == 403 && data.error == 'missing_token') {
      console.error('Server response:', data);
      alert('Error: missing token.');
    } else if (jqXHR.status == 413 && data.error == 'storage_limit_reached') {
      console.error('Server response:', data);
      alert('Error: storage limit reached.');
    } else if ((jqXHR.status == 400 || jqXHR.status == 415) && data.error == 'invalid_data') {
      console.error('Server response:', data);
      alert('Error: invalid data.');
    } else {
      console.error('Server response:', data);
      alert('Unknown error: [' + jqXHR.status + '] ' + data.error);
    }
  } else {
    console.error(jqXHR, status, errorThrown);
    alert('Error while making request. See JavaScript console for more details.');
    return;
  }
}

$(function() {
  $('#headersForm').on('submit', function(e) {
    e.preventDefault();
    return false;
  });

  $('#setupCodeForm').on('submit', function(e) {
    e.preventDefault();
    try {
      var setupObjStr = atob($('#setupCode').val());
    } catch (ex) {
      console.error(ex);
      alert('Invalid base64. See JavaScript console for more details.');
      return false;
    }
    try {
      var setupObj = JSON.parse(setupObjStr);
    } catch (ex) {
      console.error(ex);
      alert('Invalid JSON in the SetupCode. See JavaScript console for more details.');
      return false;
    }
    if (!setupObj.url) {
      alert('The SetupObject is missing an "url" key.');
      return false;
    } else if (!setupObj.token) {
      alert('The SetupObject is missing a "token" key.');
      return false;
    }
    $('#indexUrl').val(setupObj.url);
    $('#token').val(setupObj.token);
    return false;
  });

  $('#serverInfoForm').on('submit', function(e) {
    e.preventDefault();
    $('#serverInfoForm input').attr('disabled', true);
    $.ajax({
      url:  $('#indexUrl').val(),
      method: 'GET',
      json: true,
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-Gianniapp-Base-Version', $('#headerBaseVersion').val());
        xhr.setRequestHeader('X-Gianniapp-Middler-Version', $('#headerMiddlerVersion').val());
        xhr.setRequestHeader('X-Gianniapp-Middler-Name', $('#headerMiddlerName').val());
        xhr.setRequestHeader('X-Giannisync-Version', syncVersion);
      },
      complete: function() {
        $('#serverInfoForm input').removeAttr('disabled');
      },
      success: function(data) {
        if (!data.version || !data.dataEndpoint) {
          console.error('Invalid server index:', data);
          alert('Invalid server index. See JavaScript console for more details.');
          return;
        }
        $('#dataEndpoint').val(data.dataEndpoint);
        console.log('Server index:', data);
        if (data.version != syncVersion) {
          console.warn('Server reports version', data.version, 'while this tool was built for version', syncVersion);
          alert('Warning: the server reports a different version than the one this tool was built for.');
        }
      },
      error: checkErrorResponse
    })
    return false;
  });

  $('#dataEndpointForm').on('submit', function(e) {
    e.preventDefault();
    return false;
  });

  $('#dataForm').on('submit', function(e) {
    e.preventDefault();
    $('#dataForm input, #dataForm textarea').attr('disabled', true);
    $.ajax({
      url:  $('#dataEndpoint').val(),
      method: 'POST',
      data: $('#data').val(),
      json: true,
      beforeSend: function(xhr) {
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.setRequestHeader('X-Gianniapp-Base-Version', $('#headerBaseVersion').val());
        xhr.setRequestHeader('X-Gianniapp-Middler-Version', $('#headerMiddlerVersion').val());
        xhr.setRequestHeader('X-Gianniapp-Middler-Name', $('#headerMiddlerName').val());
        xhr.setRequestHeader('X-Giannisync-Token', $('#token').val());
        xhr.setRequestHeader('X-Giannisync-Version', syncVersion);
      },
      complete: function() {
        $('#dataForm input, #dataForm textarea').removeAttr('disabled');
      },
      success: function() {
        console.log('Uploaded data:', $('#data').val());
      },
      error: checkErrorResponse
    })
    return false;
  });

  $('#get').on('click', function(e) {
    e.preventDefault();
    $('#dataForm input, #dataForm textarea').attr('disabled', true);
    $.ajax({
      url:  $('#dataEndpoint').val(),
      method: 'GET',
      dataType: 'text',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-Gianniapp-Base-Version', $('#headerBaseVersion').val());
        xhr.setRequestHeader('X-Gianniapp-Middler-Version', $('#headerMiddlerVersion').val());
        xhr.setRequestHeader('X-Gianniapp-Middler-Name', $('#headerMiddlerName').val());
        xhr.setRequestHeader('X-Giannisync-Token', $('#token').val());
        xhr.setRequestHeader('X-Giannisync-Version', syncVersion);
      },
      complete: function() {
        $('#dataForm input, #dataForm textarea').removeAttr('disabled');
      },
      success: function(data) {
        console.log('Received data:', data);
        $('#data').val(data);
      },
      error: checkErrorResponse
    })
    return false;
  });

  $('#beautify').on('click', function() {
    try {
      var data = JSON.parse($('#data').val());
    } catch (ex) {
      console.error(ex);
      alert('Invalid JSON. See JavaScript console for more details.');
      return;
    }
    $('#data').val(JSON.stringify(data, null, 4));
  });

  $('#minify').on('click', function() {
    try {
      var data = JSON.parse($('#data').val());
    } catch (ex) {
      console.error(ex);
      alert('Invalid JSON. See JavaScript console for more details.');
      return;
    }
    $('#data').val(JSON.stringify(data));
  });
});